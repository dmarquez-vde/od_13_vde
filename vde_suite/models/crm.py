# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class Lead(models.Model):

    _inherit = 'crm.lead'

    is_lost = fields.Boolean(string="¿Lead Perdido?", related="stage_id.is_lost")

    def action_set_lost(self, **additional_values):
        """ Lost semantic: probability = 0 or active = False """
        stage = self.env.ref('vde_suite.vde_suite_crm_stage_lost')
        result = self.write({'is_lost':True,'stage_id': stage.id,'probability': 0, 'automated_probability': 0, **additional_values})
        self._rebuild_pls_frequency_table_threshold()
        return result

class Stage(models.Model):

    _inherit = 'crm.stage'

    is_lost = fields.Boolean(string="¿Es la Etapa Perdida?")

