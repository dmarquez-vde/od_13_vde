# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class resPartner(models.Model):

    _inherit = 'res.partner'

    user_id = fields.Many2one('res.users', string='Salesperson',
      help='The internal user in charge of this contact.', default=lambda self: self.env.user.id or False)
    razon_social = fields.Char(string="Razon Social")
    negocio = fields.Html(string="Descripción del negocio")